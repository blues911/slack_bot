<?php
/**
 * Main program.
 * Handles YouTrack users and sends Slack notifications.
 */
require_once('fn.php');

$config = require_once('config.php');
if (empty($config['youtrack']['users']) || empty($config['youtrack']['states'])) die;

$cache_file = __DIR__ . '/tmp/.cache';
$cache = [];

if (!file_exists($cache_file))
    file_put_contents($cache_file, json_encode([]));
else
    $cache = json_decode(file_get_contents($cache_file));

foreach ($config['youtrack']['users'] as $user)
{
    // prepare url
    $url = $config['youtrack']['url'];
    $url_params = 'Исполнитель: ' . str_replace(' ', '_', $user);
    $url_params = $url_params . ' Состояние: ' . implode(', ', array_map(
        function($v) {
            return '{' . $v . '}';
        }, $config['youtrack']['states'])
    );
    $url = $url . urlencode($url_params);

    // get user number of issues
    $result = fn_youtrack($config, $url);
    $res = json_decode($result, true);

    if (isset($res['value']))
    {
        if ($res['value'] === 'Unauthorized') fn_error('fn_youtrack: ' . $result);

        $key = array_search($user, $cache);

        if ((int)$res['value'] == 0)
        {
            if ($key === false)
            {
                // put user to cache and send notification
                $cache[] = $user;
                $message = '*' . $user . '* свободен для новых задач.';
                $result = fn_slack($config, $message);

                $res = json_decode($result, true);
                if (!isset($res['ok']) || $res['ok'] === false) fn_error('fn_slack: ' . $result);
            }
        }
        else
        {
            // remove user from cache
            if ($key !== false) unset($cache[$key]);
        }
    }
    else fn_error('fn_youtrack: ' . $result);
}

file_put_contents($cache_file, json_encode($cache, JSON_UNESCAPED_UNICODE));