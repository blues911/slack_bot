<?php

/**
 * Writes error into a file.
 * 
 * @param string $text
 */
function fn_error($text)
{
    $f = __DIR__ . '/tmp/error.log';
    $content = date('Y-m-d H:i:s') . ' ' . $text . PHP_EOL;

    file_put_contents($f, $content , FILE_APPEND | LOCK_EX);
    die;
}

/**
 * Returns information about user issues from YouTrack.
 * Help info: https://www.jetbrains.com/help/youtrack/standalone/Get-a-Number-of-Issues.html
 * 
 * @param array $config
 * @param string $url
 * 
 * @return string $result
 */
function fn_youtrack($config, $url)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Authorization: Bearer ' . $config['youtrack']['token'],
        'Accept: application/json',
        'Content-Type: application/json'
    ]);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

/**
 * Sends message to Slack channel.
 * Help info: https://api.slack.com/methods/chat.postMessage
 * 
 * @param array $config
 * @param string $text
 * 
 * @return string $result
 */
function fn_slack($config, $text)
{
    $ch = curl_init($config['slack']['url']);
    $data = http_build_query([
        'token' => $config['slack']['token'],
        'channel' => $config['slack']['channel'],
        'text' => $text
    ]);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}