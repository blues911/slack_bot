# Структура файлов

/tmp/ - каталог для временных файлов: .cache, error.log
/config.php - настройки
/fn.php - основные функции
/main.php - точка входа, исполняемый файл


# Настройки в config.php

slack:
url - url для отправки сообщений
token - токен доступа
channel - id канала

youtrack:
url - url для получения информации
token - токен доступа
users - массив пользователей
states - массив статусов задачи (состояние)


# Кеширование пользователей

/tmp/.cache содержит json массив пользователей у которых нет задач {'value': 0},
это нужно для исключения повторной отправки сообщений в slack. Если например,
{'value': 1} то пользователь удаляется из кеша.

Пример json: ['Тест1 Тест1', 'Тест2 Тест2']